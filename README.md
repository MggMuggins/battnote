# Battnote
The purpose of this project is to save the life of the battery in my new (sorta) System76 Galago laptop by keeping the charge level between 40% and 80% pretty much at all times. Idk if it actually works, but it was a fun project to set up with several interesting challenges.

It's based on a bash script my Dad wrote for his laptop, but I added the arduino stuff and had to rewrite it in python (bash scripts can't hold an open file descriptor...).

If there is no arduino plugged in, it plays pipe.wav if the battery is below 40% and the laptop is not plugged in, or above 80% and the laptop is plugged in. If an arduino is plugged in to `/dev/ttyUSB0` (may need to be configured via the variable in the python script), then it instead writes to the serial port if a power change is nessesary. There's an arduino sketch under `main/` in this repo for uploading to the arduino. The script _should_ work without restarting if the arduino is plugged in/unplugged in the middle of a session.

# Hardware
I bought one of [these](https://www.amazon.com/gp/product/B00WV7GMA2/) and plugged it into the arduino for the relay (level the power supply for the laptop plugged into the Normally Off plug, so that it doesn't charge when it's off). It was pretty trivial to build a little box for the arduino to screw into the end as well. Note the little green thing is actually a plug that slides out...

# Installation
I don't have this configured as a package or even have an install script because the process is too easy
1. Create a system user `battnote` (`useradd -Mr battnote`)
2. I had to add the user to `uucp` as well so that it could read/write to the arduino serial (`usermod -aG uucp battnote`)
3. Copy the `battnote.servce` into `/etc/systemd/system/`
4. Put `battnote.py` in `/user/local/bin` and make it world executable
5. Put `pipe.wav` in `/user/local/share/battnote`
6. Make sure that `/user/local/share/battnote` is `drwxrwxr-x root:battnote` (for the log file)
7. `systemctl enable battnote`
