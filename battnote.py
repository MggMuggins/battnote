#!/bin/env python

import os
from serial import Serial
import subprocess
from time import sleep, asctime

### CONFIGURATION ###

#SOUND_FILE = os.path.join(os.environ["HOME"], ".local/share/battnote/pipe.wav")
SOUND_FILE = "/usr/local/share/battnote/pipe.wav"
#LOG_FILE = os.path.join(os.environ["HOME"], ".local/logs/battnote.log")
#LOG_FILE = "/usr/local/share/battnote/current.log"

SERIAL_TTY_FILENAME = "/dev/ttyUSB0"

PULSEAUDIO = True

### END_CONFIG ###

# Can only open the serial once, because arduino resets after every close
SERIAL = None

# Keep the SERIAL global in sync with the existence of the file
def open_serial():
    global SERIAL
    
    # Check if the current serial is invalid before opening it
    #   fstat will throw if the fd is invalid, meaning the USB has
    #   been unplugged and therefore we no longer have a serial
    if SERIAL is not None:
        try:
            os.fstat(SERIAL.fileno())
        except Exception:
            SERIAL = None
    
    if SERIAL is None and os.path.exists(SERIAL_TTY_FILENAME):
        SERIAL = Serial(SERIAL_TTY_FILENAME, 9600)
        sleep(2)

# Take a string "ON" or "OFF"
def switch_power_to(mode):
    if SERIAL is None:
        if PULSEAUDIO:
            subporcess.run(["paplay", SOUND_FILE])
        else:
            subprocess.run(["aplay", SOUND_FILE])
    elif mode == "ON":
        SERIAL.write("1".encode("ascii"))
    elif mode == "OFF":
        SERIAL.write("0".encode("ascii"))

while True:
    open_serial()
    
    acpi_out = subprocess.run(["acpi", "-b"], capture_output=True, text=True)
    # Acpi out looks like:
    # Battery 0: Discharging, 79%, 01:56:12 remaining Battery 1: Discharging, 0%, rate information unavailable
    pcnt_str = acpi_out.stdout.split(", ")[1].strip("%")
    batpcnt = int(pcnt_str)
    
    power_online_file = open("/sys/class/power_supply/AC/online", "r")
    power_online = power_online_file.read().strip() == "1"
    
    log_entry = "{}\nPower online: {}\nBattery: {}%\nSerial Online: {}\n" \
                    .format(asctime(), power_online, batpcnt, SERIAL is not None)
    
    print(log_entry)
    #with open(LOG_FILE, "w") as logfile:
    #    logfile.write(log_entry)
    
    sleep_time = 180 # In seconds
    
    if batpcnt == 0:
        # Error condition
        sleep_time = 5
    elif batpcnt >= 79 and power_online:
        switch_power_to("OFF")
    elif batpcnt <= 75 and not power_online and SERIAL is not None:
        switch_power_to("ON")
    elif batpcnt <= 41 and not power_online:
        switch_power_to("ON")
    
    sleep(sleep_time)
