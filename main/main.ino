const int RELAY_SWITCH = 2;

void setup() {
    Serial.begin(9600);

    pinMode(RELAY_SWITCH, OUTPUT);
    
    digitalWrite(RELAY_SWITCH, LOW);

    while (!Serial);
}

void loop() {
    while (Serial.available() == 0) delay(1000);

    char c = Serial.read();

    switch (c) {
      case '0':
        Serial.println("Turning off Power");
        digitalWrite(RELAY_SWITCH, LOW);
        break;
      case '1':
        Serial.println("Turning on power");
        digitalWrite(RELAY_SWITCH, HIGH);
        break;
      case '\n': break;
      default:
        Serial.println("Unknown Cmd");
        break;
    }
}
